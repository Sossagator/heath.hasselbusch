# Thank you <3

This is it, the final project for DFAB.  I am grateful to have made it this far in my education.  I want to thank all my professors, family, and friends for their moral support.  

I would've quit long ago if my professors didn't believe in me and that I could accomplish these goals.  I want to thank Scott Zitek, Chris Rohal, Chris Leon, Ryan, and especially Natalie.  She inspired me to create the Beyblade launcher =)

Last but certainly not least, I want to thank my loving parents for all their love and support.  They went out of their way to help me succeed, and I can't thank them enough for all the countless nights they've spent consoling my emotions.  If I didn't have them to keep me in line, I would've gone off the rails early in my education. <3

Thank you again


> This is the end of the page
<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
     edited by <p style="font-weight: bold; color: #7851a9;">Heath Hasselbusch</p> on <a href="https://gitlab.com/lccc_ebit/dfab/2022/heath.hasselbusch" target="_blank" style="font-weight: bold; color: #7851a9;">Github</a> on <p style="font-size:13px; font-weight: heavy; margin: 5px; color: #7851a9;">3/17/2023</p>
</p>
</div>