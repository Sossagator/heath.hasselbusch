

# 2023 Spring Semester Graduation Virtual Archive

A collection of studies, findings, and digital fabrication experiments in the Graduating Spring semester of 2023 (done in Gitlab)  

## **Intro** 

***February 2nd-10th, 2023***

I spent the whole semester working on a final project to receive the credits for this class and be closer to earning my Associates in Applied Sciences & Digital Fabrication. This last project had to utilize multiple processes I learned throughout my years of learning at LCCC. While I played around with a few ideas, I finally came up with the idea of making a battery-powered, dc motor-controlled Beyblade launcher.  

Actually, I got the Beyblade launcher idea from one of LCCC Fab Lab LA's, Natalie. We were discussing what I could do, and I eventually switched gears and just started talking about something else with them. Our conversation eventually led to the topic of childhood toys and past-times. Beyblades got brought up in conversation, and she offered the idea of a motorized Beyblade launcher. I instantly went with the idea, seeing that I was struggling to even come up with an idea.

While the idea of re-vamping the Beyblade launcher isn't a new concept, I would be the first (from my extensive knowledge and research searching Google & youtube videos) to create a launcher that utilized multiple different manufacturing processes to do a quality project.

***Disclaimer(s)*** 

It took me until February 10th to get started on the Beyblade launcher concept. This is primarily due to me and my lab partner working on our group project until February 2nd. 

With this in mind, I'd like to note that each assignment tab will be about a week's worth of progress. Some weeks may be longer than others, and some weeks might end up getting grouped together, but for the time being, while I'm roughly documenting, I'll be basing my on progress pictures/videos I took (trust me I took a ton)

------------------------------------------------------------------------------------------------

When looking for the earliest evidence of researching concepts/personal drawings in my photos, the earliest image I have is from a video I watched where someone tested out a mechanical gear ratio Beyblade launcher:

![](../images/week03/Gearratio1.png)

```Date video & photo was assessed:`February 10th, 2023```

I started by taking apart an old R.C. car to understand how a motor would connect to a P.C.B. board and how a motor would drive a gear train, or in my case, control the speed and launch of a Beyblade. For reference, I took apart this Hurricane Rc Car:

![](../images/week03/hurricane.jpg)

From here, I started formulating a plan; what I'd tackle first, extensive research, etc.
Before I get too far ahead, here's my supposed B.O.M.

## Bill of Materials & More...

While this is a partial Bill of Materials, I knew that in the end, I would need more parts than I initially thought, but it could also be less. With that being said, here is my current estimated-to-be Bill of Materials (as of 3/16/2023) *=unsure regarding qty or $$

1. Jameco #23247 12V Brushed DC Motor  ```1 provided for free courtesy of Fab Lab LCCC, two extra motors purchased 3/12/2023 and received 3/16/2023`. ``
2. A4950 Motor Driver ```provided, courtesy of Fab Lab LCCC```
3. AtTINY 1614 SSN Microchip ```provided, courtesy of Fab Lab LCCC```
4. Carrera, Go! slot car remote (1/43 Scale) ```personal item```
5. 9V Battery ``` USD 4.45 per unit, current tally-3```
6. 3D printed products as needed ```provided by Fab Lab LCCC```
7. Copper Board ```provided, courtesy of Fab Lab LCCC```
8. IRFZ44N Voltage Regulator
9. 10k Resistor* x1
10. 1k Resistor* x3*
11. 4x4 Header*
12. Capacitors & Diodes are known to be needed, but unsure of what type
13. Alligator Clips ```provided, courtesy of Fab Lab LCCC```
14. Super Glue ```Dollar General```

***This list will see changes periodically***

There's only evidence proving the existence of *mechanical* Beyblade launchers, not *mechanized* ones like the one I'll be making. One guy did make one with an extremely long handle, like really long. I previously thought I would've been the first, but someone did make one with an extremely long handle. However, the problem I noticed with it was that the handle was roughly 3 feet long. Beyblades would also fly out of the arena upon contact with one another. This individual created this ten years ago, and while they did a great job on their design, I want to control how fast the Beyblades spin.

What separates my design from the rest of the competition is the electrical and motorized components of the launcher. The difference is that a mechanical Beyblade launcher wouldn't be using electircity, or by textbook definition: a characteristic of someone (or here a Beyblade launcher) that manually does labour for a living(in this case, launches a Beyblade at high R.P.M.s). You can watch that video here: 

<iframe width="560" height="315" src="https://www.youtube.com/embed/Yc0YP6YjuOA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

From here, I created the most impressive invention I've ever dreamt of creating.
From here I started formulating a plan; what I'd tackle first.

----------------------------------------------------------------------

# Week 2: Final Project Proposal, Getting GitHub Websites Ready for Editing, Brainstorming Design Ideas

***February 10th-22nd, 2023***

This week, I prepared my GitHub website to start documenting, submitted a proposal for my project, and created some sketches for my final design.  

## Getting Ready to *Propose*...

I was set on this idea from the start; I had a vision of what this would become, and that's all it took to convince me to go through with this.

Before I had to submit a thorough project proposal, I had to think about everything that would go into making this work. This means I had to identify all the machines I'd use, processes of fabrication, have a bill of materials, and obtain some collective conciseness on the basic fundamentals of this launcher, how it would work, and what potential safety measures need to be in place if things were to go south while experimenting.

![](../images/week04/proposalss.png)
![](../images/week04/proposalss2.png)
![](../images/week04/proposalss3.png)

It took me several attempts before my proposal was finally accepted for credit. You can see in blue the comments my instructor left as advice for filling out my proposal. I blame myself for getting a low grade on this for multiple reasons. 

1. I wasn't fully thinking through all the little details, and he had to comment on multiple submissions
2. I needed to incorporate more machines/machining processes to fulfill the requirements of the class.

![](../images/week04/IMG_0710.jpg)
*Exhibit A*

*Text Translation: NE555, 1k,10k resistors, Diodes, FET, Terminal blocks, 10nf 1nf capacitors, 3V LED (R, G, Y), 8 pin case, 79L12 regulator, 9V battery*

3. I needed to explain more thoroughly how everything would work.  
![](../images/week04/IMG_0711.jpg)
*Exhibit B*

This crucial bit of information kept me from reaching that next step; I had to repeatedly submit updated proposals to my instructor because I wouldn't answer everything I needed to know. This frustrated me because I felt I would address those changes on my website. I was right to have a more accurate depiction on my website rather than a submitted proposal. However, the lesson he was trying to teach was making sure I double-checked for mistakes; chances are you missed something, and that small, seemingly indescribable detail could change your plan in the long run.

## Defining the Problems

For starters, how would I power this device? Would I use DC or AC voltage to power it? What kind of motor would I need/be capable of doing what I want it to do? How will I control the speed of the motor the way I want to?

What was my PCB board going to look like/consist of? How will this device be capable of holding & violently spinning a Beyblade and releasing it at high speeds, ensuring the safety of everyone around? What if a fire breaks out, or there's a dangerous voltage, or even the Beyblade spontaneously combusts?

The only thing I was confident in was designing a 3D model, but I needed to figure out how to answer all these questions I had. 
 
I knew that I would need a motor to initiate the rotating part and a sort of switch to control that speed:

![](../images/week04/IMG_0709.jpg)
![](../images/week04/IMG_0712.jpg)

I wanted this to be a handheld design, taking after the design I saw in the video of the gear-ratio Beyblade launcher. I also pulled inspiration for the design of the handle from a TV remote I use to control a soundbar in my room: 

![](../images/week04/remote1.jpg)
This remote is for the SONY RMT-AH411U AV Soundbar System

As you may have noticed in some of my earlier sketches, there were some wonky designs that I didn't think would ergonomically make it past the drawing board, hence my decision to reference something I already own.

After a few days, I came up with a rough but presentable concept sketch:

![](../images/week04/CrazyName.jpg)

![](../images/week04/IMG_0730.jpg)

You can see on the side view how the motor would sit inside the 3D-printed base. The insert will be large enough to allow the motor to slide into place but small enough that the motor shouldn't be sitting loose enough for it to demonstrate imbalance while performing. There's a small hole meant for the shaft to stick out of, but the material surrounding the small circle is preventing the motor from falling out of place. It seems unnecessary to mention that, but it's always better to explain more than less.

Here is where I ran into my next problem:  How do I get the parts (motor, PCB board, etc...) inside and safely secure them afterward? Does the lab have a printer big enough that's capable of printing this whole thing at once?

To solve the problem of being unable to get inside the remote after it's 3D printed, I added holes on polar opposite sides of each half for screw inserts. This added feature wouldn't only make the inside accessible to add/remove components and keep everything together with all the violent spinning. Still, I could now give myself more room on the printing bed for the 3D printers. However, there was still another problem; what switch was I going to use

Evidently, this meant I had to add/change some features, like for one, my motor control method.
I originally designed to have a limit switch of some sort that when applied pressure from the thumb, it acts as a throttle to control the speed. I quickly shot this idea down. The switch I was looking for was called a momentary rocker switch, and they're close to $100, so that wasn't going to be an option.

I needed something that could control the speed of the motor while the switch was being pressed or pushed. I wasn't aware of all the switches that existed, but I found myself spending too much time on what kind of switch I was going to use. Then I got the brilliant idea of another at-home fix by utilizing what I have at my disposal: Slot cars.

Not precisely the cars themselves but the thumb joysticks used to control the car's speed. Below is a video I made to explain and demonstrate how I'll use a Carrera Go! 1/43 scale slot car remote to control the rpm of the motor in my Beyblade launcher

<iframe width="560" height="315" src="https://www.youtube.com/embed/vMHRqhHvP80" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Here's a clearer image of that same remote:

![](../images/week04/IMG_0594.jpg)

I wondered if I was thinking right about this, but my instructors assured me it would work. That microcontroller with the wires attached to it is where the magic takes place. It's a B10k potentiometer, and I determined it'll perfectly serve the new purpose I have in store for it.

I know my design of the base will change, but what I have is good for now. I have a general idea of how the bottom should look and how big/small everything needs to be, which is a step forward from where I was before.

---------------------------------

# Week 3: Gantt Chart, 3D Fastener and Beyblade Holding/Launching Apparatus in the Works

***February 23rd-March 2nd***

## Prelude:

This week I created a Gantt chart to organize myself and my thoughts. The idea behind a Gantt chart is to manage your ideas, chart a course, and stay on schedule. I went and did this, listing the weeks I'd need to focus on PCB board stuff, 3D printing, etc...but needless to say, it was challenging to work within the constraints you put yourself to in the beginning of the semester. I fell out of the Gantt chart schedule during spring break because I chose to focus on other aspects of my project. There would be no educational benefit in showing that document. However, keep in mind that this Gantt chart wasn't supposed to be followed perfectly. It still got me thinking about everything that needed to be done and helped me determine whether or not my goals were achievable.

## Goals

The first thing I needed to figure out was "how am I going to make/acquire something that'll connect to the motor's shaft, spin/shoot a Beyblade out, and be efficient enough to be capable of all that, plus not spontaneously combust during use.  

My first thought, stemming from disassembling my RC car to understand how a DC motor is powered/distributing power throughout the system, was to have a gear ratio attached to the motor shaft. The idea would be press-fitted onto the motor shaft with a gear at the end, powering another gear that was part of a holding apparatus for the Beyblade (which I still needed to get to this step), and subsequently launching the Beyblade.  

I certainly didn't want to get ahead of myself designing the PCB first... so I started de-constructing an old Beyblade launcher to understand how that worked. It was essential to figure this out first because if I didn't, how else would I know that everything would've appropriately worked, even if this was the only step I would've messed up on? After some tinkering, I learned that the screw insert hole of the original Beyblade launcher's inside gear component (shown below) was the perfect press-fit for the shaft of the motor I was working with:

![](../images/week05/22-2/IMG_0747.jpg)

## *Important Information*
Screw outer thread dimension: 2.94-2.96 millimeters
Screw inner thread dimension: 2.55 millimeters
Motor shaft dimension: 2.28-2.30 mm

That gear is what provides the force needed to launch a Beyblade with the launchers they make; the ripcord is fed through the launcher and lines up with this gear's teeth. That tall part of the geared part clicks into that clutch that sits inside the blue hooked teeth part of the launcher that actually holds the Beyblade, and there's a screw that holds that all together. Then there's a little spring-clip mechanism that, when there's no more ripcord to be pulled through the launcher, a little plastic bit that's spring loaded kicks out and stops the gear from spinning. That clutch quickly hits these walls placed inside the blue part that aids in quickly stopping the rotation of the Beyblade, effectively launching it fast enough for battle:

![](../images/week05/22-2/IMG_0704.jpg)

As you may have noticed in this image, there is no white gear part anymore. That's because I 3D printed that first white piece on an Ender 3D printer, copying the feature of the gear part that clicked into the clutch and adding another hole on the other end for a screw insert:

![](../images/week05/22-2/IMG_0703.jpg)

The one pertaining issue, though was that the capabilities of the Ender might prevent it from creating a good part to use, so lab aid Duncan printed 2 extra, scaled-up versions of my original file. It was good that we did that because it ended up being the largest (3rd) print that was suitable for testing (shown above). After applying some heat to the print to create the insert feature, we finally had something that hopefully would launch my Beyblade:

![](../images/week05/22-2/IMG_0705.jpg)

Prior to this, I'd also played around with the idea of machining a piece of metal w/ screw inserts on the side to keep everything together, tapping the motor's shaft to create a female piece that screwed onto the shaft. My issue with this was that the extra weight would really slow down the motor and not give the motor enough leeway to stop fast enough and launch the Beyblade out. This just seemed too complicated and risky overall, as opposed to just 3D printing sacrificial plastic parts that would be expendable and easily replaced after too much contortion. Below is a sketch of what the imagined fastener would've looked like had I committed to that idea:

![](../images/week05/22-2/IMG_0600.jpg)

This would only be the first of many fasteners to be printed out...not to mention the only one that really worked...but it's suspected there are other factors playing into this...but for now, at least, I have a way that my Beyblade is going to rotate as fast as I want it to so that it'll be faster than what I could personally spin it at.

## PCB...Ughhhh

Ahhh, my nemesis...PCB and electronics.

This has been my hardest task of this whole project and part of the reason why I'm not following my Gantt chart schedule anymore. XD. I struggled so hard with figuring out how the connections and everything will work. This was when I started thinking about it, but I would come to learn that this was going to take a lot more time than I originally planned for. Nonetheless, I watched a ton of Dc motor control videos, which to my dismay, mostly used 555 timers to control everything. I'm using an ATTINY 1614 microcontroller and an A4950 motor driver, and even till the end of March, I still haven't fully figured out everything, but I'm working on it...

With that in mind, I don't want to spend too much time on the electronics, seeing that I'll be making itsy bitsy baby steps every week on this part...as much as I hate it has to be this way...some things in life are uncontrollable, and you just have to deal with it and live with it.

![](../images/week05/22-2/IMG_0623.PNG)
![](../images/week05/22-2/IMG_0624.PNG)
![](../images/week05/22-2/IMG_0625.PNG)

My PCB board should use most of the components in this guy's example, but I'm still unsure about the final details.

## Moment of Truth Pt.1

So now the moment of truth...to see if my launcher will shoot out the Beyblade automatically since I'm using the same components in the original Beyblade launcher, just modified to a DC motor and not a ripcord, or if I'll need to compute some sort of code to rapidly slow down the rotation of my motor to give the Beyblade a proper launch. See the results for yourself:

<iframe width="315" height="315" src="https://www.youtube.com/embed/OeUDrC5XcPw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Honestly, I couldn't believe that it worked that way. I thought for sure I was going to have to incorporate some sort of electrical braking so that it could launch outright, but luckily I understand physics enough to know that when the voltage is dropped from the battery, the smooth ramped sides of the blue gripper cause the Beyblade to slip out once there's a change in an objects circular acceleration, in this case, the motor shaft stopping fast enough to launch the Beyblade out.

I write this down as a success.

------------------------------------------------------

# Week 4: Beyblade Launch Mechanism First Tests

***March 3rd-9th***

## Huzzah!! The First Mechanized Beyblade Launch

I can replace my old launchers with this new creation of mine because I launch multiple Beyblades at once. After coming home from my great discovery, I recorded this video demonstrating how my design works. Be sure to watch it:

<iframe width="560" height="315" src="https://www.youtube.com/embed/uSVrp-5X2yM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

I was thrilled with my result. I knew now that I didn't need to incorporate any electrical braking. Knowing this, it was definitely going to be easier programming the PCB board. I'd already started getting an idea of what this board might consist of, but still, I needed help with gaining any ground on the PCB components.

This was when I started getting a little aggravated with myself. I kept trying to force myself to understand, and I wasn't getting anywhere. I couldn't think straight for a second, with all the other things I had to worry about and all the new problems that came about the more I worked on this project.

There's not much I can say about this week. I had my focus elsewhere during this week because spring break was coming up, and there was a test for my Physics class I was stressed about (that I ended up bombing anyways because I was so damn nervous). I got myself back into more of a rhythm during spring break, and you'll start to see the gears turning again after I gave myself the time to let go and return at a better time.

-----------------------------------------------------

# Week 5: Solidworks Design for 3D Fastener & Remote Base.

***March 9th-March 23rd***

## Spring Break (3/10-3/20)

Finally...the break I needed.  

Before recording my video launching multiple Beyblades, I noticed the fastener started experiencing some warping. This deficiency caused the Beyblade to spin more unleveled when preparing to launch, and when I'd take power off, it wouldn't shoot out unless I shook it slightly. I was running myself into the ground until this point because I was so frustrated with not understanding the project's PCB concept, and not having a set Beyblade launcher wasn't making me feel any better.  

This wouldn't fly for me, and thus started the trials of 3D fasteners;

## Trials of the 3D fasteners

Though I was lucky in my first attempt at printing to make a part that worked (for as long as I needed it to work), there was no avoiding this problem. I wasn't surprised about the part deforming, but what surprised me was how fast it deformed. A slight cause of this could've been after the part was taken off the printing bed; I applied heat to the plastic to create the press-fit and manually screwed a screw in the other hole to make the threads. I believe all that contortion could've deformed the part even before the first-ever test and only worsened the more it was used. I had plenty of time to get it right; I knew my idea would work; it just needed some more tweaking.

Another leading cause of the fastener's failure could also be because of the printer. The nozzle on the Ender's are only 2mm in dimension, and seeing that my fasteners were only a few (mm) in dimension, I was told that I was fortunate enough to even come out with a clean part that worked. So I redesigned the holes so that one side was bigger~more accurate to what the press-fit hole size would be, and the other side was just the right size so when I screwed the screw in, the hole would be large enough that the screw makes its way through more straightforward, and at the same time small enough so the threads are precise.

I started with the Markforged 3D printer since the nozzle was smaller and would provide a more accurate print. My theory was I could make the press-fit hole closer to the size of the motor shaft and make the screw insert hole small enough to make precise threads but big enough so it was easier to screw it down. My first print didn't come out well because I printed it on its side, and when I printed it out the second time, it still came out pretty messy. Pictured below is my old fastener that got warped (white) and the part created by the Markforged (black):

![](../images/week07/IMG_0706.jpg)
![](../images/week07/IMG_0708.jpg)

I definitely can't trust this machine to get the job done the way I want, so I went another route with the resin printer. This method would print the parts I need to the exact dimensions with a more robust material than the Ender or Markforged.  

Here's the part that came off the resin printer:

![](../images/week07/IMG_0707.jpg)

The resin printer takes an easy win over the Markforged in terms of printing a clean part; however, when examining the Markforged print, you'll notice that the end fits nicely in the clutch, unlike the resin printer print. 

When I used the resin printer, I used the same strategy before of printing different scaled fasteners to have a larger sample size. None of these fasteners, except for the largest scaled fastener, were suitable for use until I discovered the insert failed at...inserting. I was back to square one; printing more fasteners on the Enders was my cheapest option. At least I knew it was possible to make a working part from this printer because I had done it before.  

Luckily, since the original scale was still suitable for the clutch insert (as demonstrated in the Markforged print image), I will redesign one of the ends to match the thickest diameter in the center. This will make Ender's job easier in creating the hole feature, and I can pilot the hole at the other end for the screw insert if it doesn't come out clean.

![](../images/week07/IMG_0702.jpg)
"`This one, I actually heated the plastic on the end that's visibly deformed, and while it worked for a little bit, I ultimately need a fastener that'll press right in without having to do any scraping or heating to make it work. ```
![](../images/week07/IMG_0701.jpg)

 The two shown above in the cup don't fit the clutch insert, nor were any of the holes suitable. It's a mystery as to which scaled version worked, but I believe it was the smallest one. I also managed to print one out later on that worked with the help of some superglue, but that didn't last too long either.

I at least knew by this point I could come back to this when I needed to. I could start focusing on other parts that needed to be taken care of before April came. My goal is to start really hammering the PCB board part of this project come the end of March-beginning of April. This way, I can accomplish other tasks in the meantime, just like I've said before, only this time I started designing my handle for my controller.

## Getting a *Handle* of Things

The design of the handle has been my favorite part of the project so far. I think it's cool that I'm 3D printing a remote that controls a DC motor; it's definitely not something you get the chance to do every day, that is, have access to this technology and be able to create something like this.  

![](../images/week07/IMG_0699.jpg)
"`sliced model of left/right base` "

I originally designed this to be a top and bottom half. when I superglued the fastener to the motor shaft, I didn't think at the time about how I was now going to force something of the same diameter, also connected to the motor shaft, through the bottom of this base .This meant I had to redesign it to make it left/right halves so I could just sandwich the motor launching apparatus inside. However, the superglue wore off, and since the design I printed shown above was too small, I could go back to my old design (and that makes printing way easier)

A fortunate little mishap that was all for nothing; now, if you want to read the long version, be my guest:

## Designing (& Redesigning) the 3D Base

![](../images/week07/IMG_0755.jpg)

On March 17th, I took this part off the F370. The job. Even though this wouldn't be my final iteration of my project, this sideways design provided some more insight on how I should design my second remote, as this one had a lot of problems with it
 
A glaring problem right from the start is how cramped the inside of the remote is. For one, there isn't enough room to put the motor in, or even the PCB board for that matter. Even the back holes where my speed control would connect were too small.  

![](../images/week07/IMG_0756.jpg)

Going back to the problem I had when supergluing the shaft and launching apparatus together since the glue didn't stay and the print wasn't suitable for use, it meant I could add features I previously didn't include. Some of you may be asking why I didn't just go back and redesign my old part again. Would it make sense to re-redesign that part file to its original format? No, not to me.  

Not to mention, the Solidworks lab was closed on March 23rd, so I wasn't able to do anything...I was stuck. That was until I found a way to get a license for Solidworks 2021-2022 on my laptop so I could make changes whenever I wanted to. The only problem with this is that I can't open files I used in the Solidworks lab because I was using a previous version, henceforth the need to re-create a whole base from scratch with updated features.

Some of these new features would include:
- a vent-style back to provide more ventilation for the PCB board
- redesigned motor cockpit with new ventilation and screw insert features
- smoother contours along the sides so nobody is potentially injuring themselves using this contraption
- a redesigned ramp system for the motor connection

There could be more additions to the design because I now have the luxury of doing Solidworks at home, a huge time-saving convenience for me.

## PCB

During our spring break, I met with a buddy of mine. I went to the Medina County Career Center within the Engineering Technology & Design trade, which was really good with electrical components and stuff of the sort. We sat down March 12th to discuss the matter.

Here's the datasheet of the ATTINY 1614 microchip
![](../images/week06/1614pinout.jpg)
And here's the datasheet for the A4950 motor driver
![](../images/week07/A4950.jpg)

And here, you see a brief description of what my board should look like. As I've been saying, nothing has been confirmed, but this is the best I got so far:

![](../images/week07/IMG_0753.jpg)

Some of the pinouts make sense to me, but right now, I'm still figuring it all out. I hope to meet with this friend of mine more to get more work done on it.

I do know I want to designate this circuit to run in parallel, meaning 9V of electricity is distributed throughout the entire board. I'll run aground and power train from the headers connected to the 9V battery. The IRFZ44N voltage regulator will be placed before power can reach the 1614 chip and regulate the voltage to 5V to the 1614 chip while allowing 9V to run straight to the a4950 motor driver.

--------------------------------------------------------


# Week 6: Starting to get a *Handle* of things

***March 24th-March 31st***

Finally, on March 27th, I took my print off the F370, and officially had my first prototype of the motorized Beyblade Launcher:

## Images of Beyblade Launcher Prototype Print.  

![](../images/week08/IMG_0763.jpg)
![](../images/week08/IMG_0764.jpg)
![](../images/week08/IMG_0765.jpg)

Right off the bat you'll notice a few things wrong with my print.  For one the edges are too sharp, and it just doesn't look ergonomic for anyone to hold.  The biggest drawback however, rendered the print useless; the motor hold was way too small to house the 12v motor.

![](../images/week08/IMG_0756.jpg)
![](../images/week08/IMG_0755.jpg)

Not only that, the area inside where the pcb board would sit was designed way too small.  I went back and made some changes to my design, including:

- Enlarged interior shell to provide more room for the 9V battery & PCB board
- Ventilation for PCB/9V battery (located in back of the model)
- Screw insert holes and ventilation holes on where the motor sits
- Reverting back to top/bottom half design for simplicity
- Heavy focus on making sure the holding area for the DC motor is suitable
- Ergonomic features to ensure comfort during use
- Improved dome feature

I wanted to really make this something you could just hold in your hand and *feel* the power of the Beyblade in your hands, and that the user would be confident & safe behind this mechanism.  

So I went back into Solidworks, designing a new handle that should work even without the top casing on it.  I should be able to get some footage of it all working together.

## Beyblade Launcher Mach I
On March 30th, I recieved my print(s) from the Ender Pro's. The bed size was hardly big enough to fit one of the pieces at an angle, let alone being able to print 2 large jobs at the same time probably would've taken a whole day to complete.

Instead I ran the top and bottom parts on separate machines so both can only focus on printing one thing, which also in turn reduces the time to print greatly.  I set both of them to print on the 29th around 8pm and by the time I got to lab the next morning they were complete:

![](../images/week08/IMG_0778.jpg)

*Top half is White, Bottom half is Yellow*

I really like how this came out.  I had designed the feature for the top part to slide over that skinny extrusion on the bottom part, and it fits on really well.  I almost don't need any hardware for it to stay on.  After taking it on and off so much it started loosening up.

![](../images/week08/IMG_0779.jpg)

The motor home area came out really good too; the motor slid right in, but it was a tight enough fit that it wouldn't wobble around inside.  All the holes lined up, and so I went ahead with the first round of tests.  But in classic capstone fashion, another problem was created.  See for yourself;

<iframe title="vimeo-player" src="https://player.vimeo.com/video/818126761?h=ae266d6765" width="640" height="360" frameborder="0"    allowfullscreen></iframe>

## Troubleshooting

I expected this design to work, so I was surprised when I did my first test and it made that noise.  Everything was balanced, that is except the holding apparatus for the Beyblades.  In a slow motion capture of just the apparatus spinning on the shaft while held to the body, there's a slight imbalance that can only be noticable when slowed down.  Maybe when the Beyblade was hooked on it caused that end where all the action is happening to be unbalanced; violently shaking with the load of the bey.

I tried clamping it down in a vice, playing with different voltage settings, and even drilled holes through the side 
Do I need to stabalize the shaft more? Does the motor sit in an awkward position that I couldn't see through the interior? Is there enough air going through the motor? I was feeling *stressed*, but I knew I had to deal with this problem the same way I've handled all my problems in this class; by attacking them head on and staying confident...

Nothing was making sense to me; how was it that I could use this mechanism outside of the case, but not when it's tightly secured in it's own area.  My hypothesis was that perhaps, since there isn't any air movement within the base where the motor sits, it's almost like the motor is suffocating itself inside the printed base.  

When taken out of the base, there's no restriction to it's airflow, and if you look inside the yellow base you'll notice a lack of air holes *around* the motor.  I accounted for ventilation where the motor would be seated, but didn't think that restricting air flow could cause the motor to lose efficency.

With this in mind, I decided to start experimenting with different ways of holding the motor/launching the Beyblade.

## Experimenting 

In my first experiment, I placed the motor vertically into the base, this time flipping the yellow base upside down so its hanging down, not inside the body.  When I did this, it launched the Beyblade as it would've if I held the motor in my hand:

<iframe title="vimeo-player" src="https://player.vimeo.com/video/818136480?h=d7d75553b7" width="640" height="360" frameborder="0"    allowfullscreen></iframe>

Right away you can see and hear the difference in the launches.  Compare this footage to the previous close-up footage showing how unbalanced it was.  This time it's quieter, gets up to the speed I need (with a 9v battery I should metion) and launches it like it would've launched if I was just holding the motor (like before).  I believe the reason for it being so loud when the motor is oriented the way it's supposed to be, is because of how cramped the motor was inside that hole.

This meant that it wasn't the design of the base entirely, just where the motor was sitting; proving that all I might need are some airways.

My other theory; was that maybe that hole I made for the motor to fit in is still really claustrophobic.  That, along with the lack of ventilation around the motor might be playing hand in hand in restricting the efficeny of the motor.  So in my second experiment; my physics instructor and I decided to test whether or not pressure around a motor effects it's speed.

To test this, we grabbed a motor he had in the backroom and connected it to power; recording the fastly rotating shaft's frequency with a sensor to detect any sort of resistance. The highere the frequency, the higher the resistance.  My hypothesis is that when we put the motor under stress, the frequency will be higher than the other tests because the components inside are being compressed.

Based on the motor's orientation, and surrounding pressure, here are our findings:

![](../images/week08/IMG_0898.jpg)


In the first 2 trials, we ran the motor upside down and right side up (right side up meaning shaft pointing down).  The distinction between the 2 frequencies weren't all that surprising, however a blurb in the right side up trial peaked our interest; maybe when the motor faces down it experiences some resistance from something inside the motor sitting on top of something else perhaps?

We ran another test, but this time didn't get that hiccup in the data.  We suspect this hiccup in the first trial could've been some external force that the sensor picked up.

But what we found interesting was the data collected from when pressure was applied to the outside of the motor.

In this trial, we ran another test holding the motor in it's downward position to determine if that unexpected blurb was truly a hiccup, or part of the motor:

![](../images/week08/IMG_0899.jpg)

Turns out, it was just a hiccup.  Something else must've been detected on the sensor, however what WAS true was that applying pressure on the motor DID make a difference in the performance of the motor.

Granted the motor we used wasn't one I planned on using in my project, and we didn't attach the beyblade apparatus to the end, but the logic should still hold true that encasing a motor in a tight area without anywhere for the air to escape/move through can affect it's performance.

Next week, we'll have a new bottom base, with some updated features.  Hopefully I get a start on the PCB stuff because I'm only about a month and a few weeks away from graduation.  I'm glad I was able to prove my hypothesis correct through scientific experimentation, and if all goes according to plan, this new base should be much more stable, quiet, and potentially capable of achieving higher rpm's.

-------------------------------------------------------------

# Week 7: Making Progress

***April 1st-April 7th***

## Beyblade Launcher Mach II

With the evidence I collected, I got back to work designing another bottom base, this time allocating for the following:

- Increased inner diamater inside motor hold
- Larger holes for cords to fit through back end
- Enhanced ventilation surrounding motor area
- Rounded edges to improve ergonomics

This time all I needed to print is the bottom base; the top part should fit on the same, if not better because I changed all the fillet radiuses to be the same.  The print took 13 hours this time, and once again I took it off the Ender bed on lab Thursday the 6th.  This time however, I got a much different result:

<iframe width="560" height="315" src="https://www.youtube.com/embed/sTZYGZ1Y8U8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

It works!!! I almost couldn't believe it, but I had to.  All my hard work and experimenting payed off, and I can finally say that I have a working prototype.

The base is noticably more stable. Now that there's some airflow inside and the motor isn't squeezed in there against the PLA plastic, I can launch all my Beyblades at 3x the speed I was before; this was spinning way faster than when I had it connected to a 9V battery.  

Granted I still haven't ran a test with the top part screwed on, but it's because the alligator clips I used for the connections don't exactly hold well together, and wiggle out of place after a few launches.  Still, as long as I could use it this way, all that's left to do now is design...yes...the PCB board that'll go inside.

Here's another video showing me launching all my Bey's at once:

<iframe width="560" height="315" src="https://www.youtube.com/embed/u1-tWvzarn8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

This works great! These things spin like crazy and hold pretty well while getting up to speed.  I've negated the time it took to put the ripcord through the launcher, and I can launch them all with ease, not to mention 3x faster than they're supposed to XD

## PCB (Prelude)

It took me a good 2 weeks to come up with a board, and during this week I focused on researching more boards and using other peoples schematics to come up with my own

Starting off the list of changes to my components of my pcb board; I don't think I'll be using a 9V battery anymore.  I would be changing them batteries every time I used this thing, so I switched tactics and decided I was going to use a wall outlet now to power my circuit.  It'll provide a more powerful, consistent source of energy without me having to buy/use a bulky voltage supply to power this thing.  

I also won't be putting led lights on to indicate the speed of the motor, but this would be a design feature I'd include in my 2nd launcher that I'll make in the future ;) It would've been too much to work around and my pcb board got crowded quick.  You'll see later why I had to dumb down my board because of time constraints.

I'll say this right now, it was a lot easier coming back to the pcb board aspect after I'd figured out the rest of my project.  Truth be told, I don't think I could've started out with designing the board because if I did that before creating a prototype, it'd be like working in reverse.  How would I know if the chip would work if I wasn't able to replicate what I'm trying to achieve.  Now that everything else was out of the way, I was thinking much more clearly about my board.

I'll end this portion here, but next week is going to have all my PCB board stuff


---------------------------------------------------------------

# Week 8: PCB...For Real This Time

***Arpil 8th-April 14th***

Now...it's time: Making the PCB board.

## Interlude

This without a doubt was the hardest part of the whole project.  I've already had experience with 3D printing, and I've been doing this Gitlab documentation so much that I've started to get comfortable doing and figuring out new ways of coding a website.  This however would be a big step out of my comfort zone.

I've learned about electric components and microcontrollers in the last 2 years I've been at LCCC, but needless to say I wish I played with electricity more before this.  I created PCB boards in my fall semester of 2022 (all my documentation from that semester can be found in week 2 titled "Virtual Archive").

I've expressed my concern over the faitful day that is today; getting around to creating the board.  Mostly this is because I was so stressd out thinking about everything else I had to do in my project, that I didn't stop to just focus on one aspect first, before moving on to the next.

Well now we're here, and truthfully, I'm much more composed and confident in my ability to create this.  

More often than not in life we find ourselves putting on our horse blinders to focus on accomplishing a task.  That's the case for me anyways.  When a million thoughts and ideas start colliding in your brain, it can seem impossible to accomplish that task because of how many steps it's gonna take to get there.  I had to teach myself to disect problems into it's smaller parts, in order to better understand how everything comes together.

I'm definately one who looks at a problem and tries to solve it in whole.  By conditioning myself to tackle one thing at a time, I found it easier to generate concepts without getting mixed up thinking about what's next.

## Getting Started

Once again, I took to the internet to find examples of people using a4950 motor drivers, attiny 1614's, and/or controlling a 12v DC motor.  Instead of looking for someone who used these exact components all together, I observed how people were pinning each individual component, and attributing those connections with my schematic.

Using that 9v battery was convenient, but not quite strong enough to achieve the speeds I'm looking for.  This being said, the circuit I made to control this launch sequence was a simple button switch circuit, connected to the voltage supply and dc motor.  This also made me re-think the idea of using a potentiometer from an old slot car remote of mine; the B10k potentiometer was more complicated under the surface of just seeing it as a speed controller.  In turn, I'm going to now use a button switch (similar to the circuit I used to launch the Beyblades from the red base).  Now instead of being able to variable control the speed, when I press down the button it'll output between 10v-12v (depending on how powerful I want this to be) and bring the Beyblade up to speed.

I wish I could use that B10k potentiometer, however it would've created a bigger, yet unnessecary problem when all I need to do is get it up to speed.  I'd rather just press and release a button to launch a Beyblade, rather than have the ability to launch the Beyblades slower than I intended to launch them, kinda defeats the whole purpose of this concept in my eyes.

Here is my updated B.O.M. for the PCB board:
*Disclaimer, all parts were attainable through LCCC's Fab Lab

- (1x) ATTiny 1614 
- (1x) A4950 motor driver
- (1x) LDA Voltage Regulator
- (1x) 3-pin connector for programming Arduino
- (2x) 2x3 pin headers, 1 for the button switch, the other to transfer data from the motor driver to the 12v motor
- (1x) Green LED to indicate power
- (1x) 1k Ω resistor
- (1x) 1uf capacitor
- (1x) 2.1mm conn-jack


## Designing the PCB

Here's my first iteration of my PCB board:
![](../images/week10/schematic.png)

![](../images/week10/oldboard.png)

This design was too large to be cut on the copper stock we had, not only that, but it was faced horizontal.  Why is this a problem you may ask?  It's because the sacraficial material on the Roland MDX-50 is facing vertical.  At first I couldn't figure out how to rotate everything; I thought I could only rotate individual components.  I was fortunately mistaken; to rotate a group of components in eagle all you do is group select everything, select the move icon, right click to rotate.

After rotating the board, I went to cut it out.  Then...another problem reared it's ugly head.  When I saw ugly, I really mean it.

![](../images/week10/IMG_0864.jpg)

Despite setting my origins at different points, it continued to account for something in the file that wasn't supposed to be there.  I worked with Chris Leon to figure this problem out, and we discovered the text for the 3pin connector was way off to the side, and so that's what was causing the issue with setting the origin.  

![](../images/week10/board.png)

After deleting the text (yes I know it's shown here but I deleted it) it was still registering the white part of the 3pin connector, but I wasn't worried about the MDX-50 cutting this feature out because even though it somehow made it's way through (despite me hiding the layer) the mill is smart enough to not cut invisible things.

![](../images/week10/generated_traces.png)
![](../images/week10/generated_outline.png)

I made some more modifications, mostly making the board more compact to fit the size of the sacraficial.  

Throughout all this time I thought the design of the board was going to be the hardest part, boy I was horribly wrong.

## Sodering

So here's the board with all the components I'll use:

![](../images/week10/IMG_0865.jpg)

Here's another view of the traces:

![](../images/week10/traces.png)

I didn't know at the time, but it took me 4 days worth of cutting out boards, attempting to soder, and repeatedly failing over and over and over again.  I nearly lost my sanity because every time I had to cut a new board, that meant I had to 0 the z-axis for both tools (1/64 bit and 1/32 bit).  Alone the process of setting up the origins and just getting the job ready to cut took 5-10 min each time.  Then the cutting time for the traces took about 15-20 min.  

So I had to do one job where I 0-ed the 1/64 bit and cut out the traces, then I had to change the tool to the 1/32, re-zero it's z-plane, and just hope the double sided tape holds on long enough so the board doesn't get damaged while cutting.

I must've cut out nearly or a few more than 10 boards between Monday and Thursday.  2 of them included the text I mentioned before so those were completely unusable, 4 of them had traces break on me when exacto-kniving the remaining copper, 2 boards had their pads ripped up because I'd apply too much heat when sodering.  I attempted to fix the traces when I broke them by taking some copper tape and sodering it over the broken trace, but I just ended up making more work for myself in the long run doing it that way.  

One board in particular really broke me down emotionally because I had it all sodered together, and when I plugged it in, I heard a sizzle and saw a puff of smoke come out of the 2.1mm power jack...I shorted the circuit.  That was about halfway through all my trials, so I failed on sodering and trimming the copper on other occasions after that.  
![](../images/week10/IMG_0867.jpg)

I spent all day Thursday cutting out boards too, hoping I'd come home with at least something.  Starting to run out of copper (and time) I said to hell with making it look pretty & taking off that extra copper, and proceeded to my 10th (and final) cut.  

Both of the bits on the Roland MDX-50 broke on me during use, especially when the 1/32 one broke as it was cutting a board...that one got messed up.  I had to change the tools and hope that I can just hurry up and get this board already.

Alas...the pcb board came off the mill, I didn't exacto-knife the edges, and went right to sodering.  

Without further ado...I give you:

![](../images/week10/IMG_0901.jpg)
![](../images/week10/IMG_0902.jpg)
Yes...the pcb board.

What a monumental moment.  This was undoubtedly my biggest challenge of this whole project, but I stuck with it and didn't give up until I had what I wanted.

So for now the only thing left to do is code the program in Arduino and make some stickers to put on for decoration.

It's all coming together.....


--------------------------------------------------------


# Week 9: Planning Vinyl Cutting, Arduino Coding

***April 15th-April 22nd***

With the downtime I had, I documented everything from March 24th till today.  In the meantime, I took osme shots of the yellow and red bases to compare them to eachother:

![](../images/week11/IMG_0894.jpg)
![](../images/week11/IMG_0893.jpg)
![](../images/week11/IMG_0895.jpg)
![](../images/week11/IMG_0896.jpg)

From this perspective you can see the inside of the red base, and the hollowed out area to give the motor some more breathing room.

Speaking of breathing, notice the ventilation holes that go around the inner diameter.  This design truly solved the issues I was having before, and all it took was giving the motor some space...something you probably shouldn't say to your significant other.

I intend on printing one last bottom part.  This time with the following chagnes:

- Enlarged/Repositioned screw insert holes (not lined up right in both prints)
- Widening the area where the pcb board will sit because currently the space is barely too narrow

The one thing I'll have to add to the top base is a new design that allows a button switch to rest on top of the top plate.

I also want to make a piece that slides onto the bottom of the base.  A curved barrier that provides more protection to the user so if something disconnets while spinning, your little fingies should be spared.

## Beyblade Launcher Mark III

For my new launcher design, I wanted to make the following changes:

- More space for the PCB board to fit in
- Modified screw insert holes
- Designated area for button to sit on top piece

I did not print out a safety guard.

![](../images/week12/IMG_0917.jpg)
![](../images/week12/IMG_0922.jpg)

I printed out a new model with all the concepts above.  However, I think I used a poorly-calibrated printer because the quality of the launcher was terrible:

![](../images/week12/IMG_0931.jpg)
![](../images/week12/IMG_0935.jpg)

The high, thin walls where the top case secures into place was under extruted; meaning the filimant didn't fill as much as it should've.  Not long after playing around with it, the sides started to break away.  Looking at my design you'll see the little spaces I put underneath the edges of that high part made a really thin geometry; so thin the 3D printer couldn't print any finer to create the feature.

To my surprise, this model actually launched the Beyblade like it was supposed to:

<iframe width="560" height="315" src="https://www.youtube.com/embed/zX1mxFuYGnw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

I decided to leave the device alone, since I couldn't get my original launcher to work again for some unknown reason, I didn't want to risk breaking it by showing it off to someone or carrying it around with me everywhere I go.

Now I can move on to the other parts of my project that aren't so mechanical.

> This is the end of the page

---------------------------------------------------

# Week 10: Vinyl Designs, CNC Arena Cutout, and More PCB...

***April 23rd-April 30th***

##  Intro

With the end of the year nearing, I really need to kick it into gear with my pcb part.  I keep getting hung up on how I'll control the 12V thats supposed to power the motor, when the potentiometer is recieving only 3.3V from the voltage regulator.  


![](../images/week12/IMG_1009.jpg)
![](../images/week12/IMG_1012.jpg)
![](../images/week12/IMG_1013.jpg)
![](../images/week12/IMG_1014.jpg)
![](../images/week12/IMG_1015.jpg)

To my dismay, many of these boards I've cut out, soldered, and attempted programming have all failed.  This design included.  Most of them have been problems that occured while soldering and cutting off extra material which can cause shorts.  I failed multiple times; I cut traces with the exacto knife on accident, pulled up pads by heating them too much with the solder gun, and on a few occasions the mdx-50 mill was the cause of failure.  Even if I made a circuit without messing anything up, 2 of them shorted on me when I plugged them in.  The one that never shorted while plugging in; shorted after programming.  That is the circuit you see in the diagram below.
![](../images/week12/drawn_board.png)
![](../images/week12/LDA-LM3480.png)
The reason for this board's failure is attributed to the voltage regulator.  The pins in the schematic don't match the pins in the datasheet for the LDA-LM3480 voltage regulator.  Looking at the schematic, you'll see the pin numbers are switched:
![](../images/week12/wrongtraces.png)

This mistake caused the ATTINY 1614 to overheat, but I was running out of time to meet the other requirements of my project.

##  Vinyl & CNC

I used the roland gs-24 vinyl cutter to cut out my designs.  I'll print some more out on May 8th before the project is due.  All I'm doing is Beyblade text on the top part of the launcher with flames around the side of the bottom part.

The other requirement I stated I'd meet was to cut out something on the Forest Scientific CNC Router.  My plan is to cut a 2ft by 2ft piece that I can screw my arena onto, and have a slot in the front for my launcher to fit nice and snug.

![](../images/week12/IMG_0990.jpg)

![](../images/week12/IMG_1018.jpg)

The job took not even 5 min.  You can actually see the Beyblade vinyl text on the top of the launcher.

Now all that's left is to figure out this darn pcb board.  That and print out another handle that'll look sleek.


----------------------------------------------------------

# Week 11: May the 4th Be With (Me)

***May 1st-May 6th***

## Very Little to Hope for....

With only a week left to work on these projects, I had to go into hyperdrive to finish these last few tasks. 

I made a new launcher design with the following changes:

- Sunken in part for PCB board to fit in
- Lined up sides of walls to prevent under-extrusion again & created enough space to place pcb board in
- Added potentiometer section on top part

I also made a new PCB board with the following changes:
- 3 More capacitors (including 1 10uf cap to the motor driver)
- Traces Vref to 3.3V. LSS to motor drier thermal pad & ground
- Utilized more pins for voltage and ground.

![](../images/week13/NewLayoutDrawing.png)
![](../images/week13/5.2.23Generatedoutline.png)
![](../images/week13/5.2.23Generatedtraces.png)


![](../images/week13/IMG_1050.jpg)
![](../images/week13/IMG_1051.jpg)

This time, I was able to get the PCB board blinking!

<iframe width="560" height="315" src="https://www.youtube.com/embed/AQBo2JeD1bw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>


However, something is still wrong with my board.  I'm having trouble getting the button and potentiometer to communicate with the 1614.  I tried getting the button to control the led turning on/off and other simple programs, and yet couldn't figure out what wasn't working.  I had breadboarded the circuit beforehand and copied that connection on the board, but it still didn't work.  It's looking grim for the PCB board...

In similar fashion, the green print that hasn't been touched in over a week started acting funny like the red one did.  The new print also did the same wobble.  

![](../images/week13/IMG_1073.jpg)
![](../images/week13/IMG_1074.jpg)
![](../images/week13/IMG_1075.jpg)

This outraged me.  I've worked so hard on this project for so long, only to come up short.  I don't think I'll have enough time to finish this project properly because I've encountered hiccups like this throughtout the whole process, and it doesn't help that my knowledge of electronics is lacking considering I took classes on them.  

Despite all this however, I give myself credit for at least trying and figuring out all the problems.  I can't explain why the button and potentiometer don't work on the board, nor can I explain why these launcher prints are not consistent with properly holding the motor while spinning the Beyblade.  I can confirm however that after multiple tests, I know that my 3D printed apparatuses have been functional this whole time; I never have a problem using this setup when it's outside of the 3D printed handle.  



I guess this is it for now.  
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣀⡀⠀⠀⠀⢀⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣤⣶⣾⣿⡉⢤⣍⡓⢶⣶⣦⣤⣉⠒⠤⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣴⣿⣿⣿⣿⣿⣿⣷⡀⠙⣿⣷⣌⠻⣿⣿⣿⣶⣌⢳⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⣰⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣄⠈⢿⣿⡆⠹⣿⣿⣿⣿⣷⣿⡀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⣰⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣄⠹⣿⡄⢻⣿⣿⣿⣿⣿⣧⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⢠⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠿⠿⣿⣿⣷⣽⣷⢸⣿⡿⣿⡿⠿⠿⣆⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡄⠀⠀⠀⠐⠾⢭⣭⡼⠟⠃⣤⡆⠘⢟⢺⣦⡀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⢛⣥⣶⠾⠿⠛⠳⠶⢬⡁⠀⠀⠘⣃⠤⠤⠤⢍⠻⡄⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⡿⣫⣾⡿⢋⣥⣶⣿⠿⢿⣿⣿⣿⠩⠭⢽⣷⡾⢿⣿⣦⢱⡹⡀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⣿⣿⣿⣿⣿⣿⡟⠈⠛⠏⠰⢿⣿⣿⣧⣤⣼⣿⣿⣿⡏⠩⠽⣿⣀⣼⣿⣿⢻⣷⢡⠀⠀⠀
⠀⠀⠀⠀⠀⢰⣿⣿⣿⣿⣿⣿⢁⣿⣷⣦⡀⠀⠉⠙⠛⠛⠛⠋⠁⠙⢻⡆⠀⢌⣉⠉⠉⠀⠸⣿⣇⠆⠀⠀
⠀⠀⠀⠀⢀⣾⣿⣿⣿⣿⣿⡇⢸⣿⣿⣿⣿⠷⣄⢠⣶⣾⣿⣿⣿⣿⣿⠁⠀⠀⢿⣿⣿⣿⣷⠈⣿⠸⡀⠀
⠀⠀⠀⠀⣼⣿⣿⣿⣿⣿⣿⠀⣌⡛⠿⣿⣿⠀⠈⢧⢿⣿⡿⠟⠋⠉⣠⣤⣤⣤⣄⠙⢿⣿⠏⣰⣿⡇⢇⠀
⠀⠀⠀⣼⣿⣿⣿⣿⣿⣿⡇⢸⣿⣿⣶⣤⣙⠣⢀⠈⠘⠏⠀⠀⢀⣴⢹⡏⢻⡏⣿⣷⣄⠉⢸⣿⣿⣷⠸⡄
⠀⠀⣸⣿⣿⣿⣿⣿⣿⣿⠁⣾⣟⣛⠛⠛⠻⠿⠶⠬⠔⠀⣠⡶⠋⠿⠈⠷⠸⠇⠻⠏⠻⠆⣀⢿⣿⣿⡄⢇
⠀⢰⣿⣿⣿⣿⠿⠿⠛⠋⣰⣿⣿⣿⣿⠿⠿⠿⠒⠒⠂⠀⢨⡤⢶⣶⣶⣶⣶⣶⣶⣶⣶⠆⠃⣀⣿⣿⡇⣸
⢀⣿⣿⠿⠋⣡⣤⣶⣾⣿⣿⣿⡟⠁⠀⣠⣤⣴⣶⣶⣾⣿⣿⣷⡈⢿⣿⣿⣿⣿⠿⠛⣡⣴⣿⣿⣿⣿⠟⠁
⣼⠋⢁⣴⣿⣿⣿⣿⣿⣿⣿⣿⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣎⠻⠟⠋⣠⣴⣿⣿⣿⣿⠿⠋⠁⠀⠀
⢿⣷⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⣴⠀⠻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⣠⣾⣿⠿⠿⠟⠋⠁⠀⠀⠀⠀⠀
⠀⠉⠛⠛⠿⠿⠿⢿⣿⣿⣿⣵⣾⣿⣧⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠉⠉⠉⠉⠉⠉⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀


----------------------------------------------------------------


# Week 12: May 7th-May 13th

On May 9th I presented my project to my instructor Scott Zitek.  While most of my project was good, I didn't accomplish motor control from my chip.  Even though my board works as shown below: It hasn't started working the motor yet.  This is obviously a big part of my project, but so big that it could cause my entire project to be all for nothing if this doesn't work...

<iframe width="560" height="315" src="https://www.youtube.com/embed/hPhUi041g1c" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/et7CbTJEGAA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/NGX_lzeQbCU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/o40wkxr32Sc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/QZYe0NTKCQo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

I'm not sure if this is going to be taken for credit or not, but I'm trying to get with one of the instructors before the semester ends to get that figured out.

## Final B.O.M.

1. Jameco #23247 12V Brushed DC Motor  ```1 provided for free courtesy of Fab Lab LCCC, two extra motors purchased 3/12/2023 and received 3/16/2023. ``` [Jameco #23247 Motor](https://www.jameco.com/z/MD5-2445-Jameco-ReliaPro-12-Volt-DC-Motor-19-850-RPM_232047.html)
2. A4950 Motor Driver ```provided, courtesy of Fab Lab LCCC``` [A4950 Digi-Key](https://www.digikey.com/en/product-highlight/a/allegro-microsystems/a4950-dmos-full-bridge-motor-driver-ic)
3. AtTINY 1614 SSN Microchip ```provided, courtesy of Fab Lab LCCC```  [ATTINY 1614 Digi-Key](https://www.digikey.com/en/products/detail/microchip-technology/ATTINY1614-SSNR/7354616)
4. Voltage Supply ```provided, courtesy of Fab Lab LCCC```
5. 3D printed products as needed ```provided, courtesy of Fab Lab LCCC```
6. Copper Board ```provided, courtesy of Fab Lab LCCC```[Copper Stock](https://www.googleadservices.com/pagead/aclk?sa=L&ai=DChcSEwizuLDY5-r-AhXufG8EHfBLA04YABAMGgJqZg&ase=2&ohost=www.google.com&cid=CAESauD20YSQSI5MmLaAd1ROZgKj7XThBiM945cO1TnvDRn9qUUTsZ8iF9e1o-sgaqIdTUFaLSCB_rJfC5YAcbLcJjn4lTNRfHcs9xZhhJFLKW14aFbeGyawUsabrDnYXikQ1sBwXW8aY0xbLI0&sig=AOD64_3Fc6oKjt6ZFBJ2rHWNkxw4Qlj9AQ&ctype=5&q=&nis=4&ved=2ahUKEwjR1anY5-r-AhWVJUQIHTFMCBUQ9aACKAB6BAgCEA4&adurl=)
7. LDA-LM4380 3.3 Volt Regulator  ```provided, courtesy of Fab Lab LCCC``` [LDA Regulator Digi-Key](https://www.digikey.com/en/products/detail/texas-instruments/LM3480IM3X-3-3/3697028?utm_adgroup=Texas%20Instruments&utm_source=google&utm_medium=cpc&utm_campaign=Dynamic%20Search_EN_Focus%20Suppliers&utm_term=&utm_content=Texas%20Instruments&gclid=CjwKCAjwge2iBhBBEiwAfXDBR97QOpRHifYJewcfVuZjUz7fZZqjhJSks2XkLdf1OHSsmX41adUJmhoCyaAQAvD_BwE)
8. (x3) .1 uf Capacitors
9. (x1) 10 uf Capacitor
10. 2.1 mm Comm Jack ```provided, courtesy of Fab Lab LCCC``` 
11. (x3) 2pk of Super Glue ```Dollar General, dontated to DFAB LAB afterwards```
12. (x2) 6 Pin Headers ```provided, courtesy of Fab Lab LCCC```
13. (x1) 3 Pin Connector  ```provided, courtesy of Fab Lab LCCC```
13. (x1) 1k Resistor  ```provided, courtesy of Fab Lab LCCC```
14. (x1) Green LED  ```provided, courtesy of Fab Lab LCCC```

I ended up graduating after all :)

> This is the end of the page
<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
     edited by <p style="font-weight: bold; color: #7851a9;">Heath Hasselbusch</p> on <a href="https://gitlab.com/lccc_ebit/dfab/2022/heath.hasselbusch" target="_blank" style="font-weight: bold; color: #7851a9;">Github</a> on <p style="font-size:13px; font-weight: heavy; margin: 5px; color: #7851a9;">3/17/2023</p>
</p>
</div>
