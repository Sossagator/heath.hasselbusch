# About me

Greeting's viewers,  Allow me to introduce myself:
 
![](../images/bridge45.jpg)

My name is Heath Hasselbusch.  I'm a student at Lorain County Community College, looking to graduate in the spring semester of 2023.

Here's a brief history of what I've accomplished in my engineering career

## My background

I currently reside in Medina County, where I've spent my whole life.  I did Track & Field for my alma mater Brunswick High School.  I attended the Medina County Career Center for Engineering.  I've always had hopes of being an engineer because I love making things.  Lego's and Minecraft were some of my favorite imagination-fueling past times.  Growing up in the era of 3D printing is an exciting prospect for me because the capabilities of 3D printing are near limitless, and I want to test those boundaries as a career. 

## Previous work

I've made countless objects in the years I've been learning about additive manufacturing, but any time I make something, it's with the intent of it being put in my room and having some practicality.  Some examples of this include my Vans shoe rack that I made in the spring semester of 2022...however it didn't turn out so well because my digital joints weren't cut out to the right size...and this resulted in a very unstable shoe rack.  I did manage to get it in the right spot so it stays together:

![](../images/index/ShoeRack.jpg)

I made this DJ Marshmello figurine helmet during my time at the Medina County Career Center:

![](../images/index/Marshmallo.jpg) ![](../images/index/Ride&Die.jpg) 

It's hard to make out, but I inscribed names of my close friends in the Engineering trade, along with the names of some (now former) other friends of mine.  

In the Fall semester of 2022, I created multiple pcb boards as part of DFAB 221's lecture/learning curriculum:

![](../images/week05/finalPCB.jpg)

This here was the first pcb board I created.  This chip I made helped me program my other pcb boards.  Without this, the other microchips I made wouldn't exist, or work as they're shown.

This was created, with just a stock piece of copper, a Roland MDX-50 CNC mill (cutting process shown below)

![](../images/week05/newbase.jpg)

some electrical components, and a bit of soldering and Arduino programming later I had my first microchip:

![](../images/week05/finalPCB.jpg)

With this piece of technology I just made, I went on to make some other pcb boards:

![](../images/week07/after_sodering.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/wVH2pZWBI14" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

![](../images/week11/heroshot2.jpg)
![](../images/week11/Arduino_Code.jpg)


<iframe width="560" height="315" src="https://www.youtube.com/embed/IAu4KcermpE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>


> This is the end of the page
<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
     edited by <p style="font-weight: bold; color: #7851a9;">Heath Hasselbusch</p> on <a href="https://gitlab.com/lccc_ebit/dfab/2022/heath.hasselbusch" target="_blank" style="font-weight: bold; color: #7851a9;">Github</a> on <p style="font-size:13px; font-weight: heavy; margin: 5px; color: #7851a9;">3/17/2023</p>
</p>
</div>